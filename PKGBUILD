# Maintainer: tioguda <guda.flavio@gmail.com>

pkgname=softplan-websigner
_pkgver=2.9.5
pkgver=${_pkgver}.1
pkgrel=2.2
pkgdesc="The Web Signer native application. An easy solution for using digital certificates in Web applications."
arch=('i686' 'x86_64')
url="https://websigner.softplan.com.br"
license=('custom')
groups=('')
options=('!strip' '!emptydirs')
makedepends=('imagemagick')
install=${pkgname}.install
source=("https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}.png")
source_i686=("https://websigner.softplan.com.br/Downloads/${_pkgver}/webpki-chrome-32-deb")
source_x86_64=("https://websigner.softplan.com.br/Downloads/${_pkgver}/webpki-chrome-64-deb")
sha512sums=('de7aada006b8c4a448e88b30e99695415263a5a899331173f614736068f64cc7be4f2f1044717531fc2807a661af162ec5aedddcc7a0a71dba027315b1b8c7f0'
            '2cee991fbe4356be41e41ecdfdb3d66c9b5ad8d86fd58403666b294f72ba8ea502ee30763b7160fea0b40d0284328b763779ac06417bdde59c99f7a38619ee17')
sha512sums_i686=('c5d9a30f1a93fa1f75ef10e9632ed6d30b59a19fb1b50dbdd27f6effd6a841f1f903a790a21cdde4f7713dfc745961e0477091f7e321c73f508a5649658ee023')
sha512sums_x86_64=('a57b50c3f6f8f38282b4b1f9a39f7447c9fd87d4e1da549248e9657c201502cd9db035a3e59e7f3326e46a903ae7f6973e7e084b6fb2f38c3944079cdcd71372')

_softplan_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_softplan_desktop" | tee com.${pkgname/-/_}.desktop
}

package(){
    depends=('desktop-file-utils' 'glib2' 'gtk3>=3.6' 'hicolor-icon-theme' 'xdg-utils')

    # Extract package data
    tar xf data.tar.xz -C "${pkgdir}"

    if [[ ${CARCH} == x86_64 ]]; then
        rm -rf "${pkgdir}"/usr/lib/*; mv "${pkgdir}"/usr/lib64/* "${pkgdir}"/usr/lib; rm -rf "${pkgdir}"/usr/lib64
    fi

    if [[ ${CARCH} == i686 ]]; then
        rm -rf "${pkgdir}"/usr/lib64/*
    fi

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" \
        "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/${pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done

    # Archify folder permissions
    cd ${pkgdir}
    for d in $(find . -type d); do
        chmod 755 $d
    done
}
